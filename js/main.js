function findGetParameter(parameterName) {
  var result = null,
    tmp = [];
  location.search
    .substr(1)
    .split("&")
    .forEach(function(item) {
      tmp = item.split("=");
      if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
  return result;
}

$(document).ready(function() {
  var aid = findGetParameter("aid");
  var pid = findGetParameter("pid");
  var cid = findGetParameter("cid");
  if ( findGetParameter("email") != null && findGetParameter("email").length>0 ) $('#email').val(findGetParameter("email"));

  $(document).on("change", ".api-check-disabled", function() {
    console.log("change");

    var dhis = $(this);
    var check = $(this).attr("data-check");
    var val = $(this).val();
    var data;

    switch (check) {
      case "zipcode&land":
        // zipcode&land ist besonders
        var valZip = $(".api-zip").val();
        var valLand = $(".api-land").val();
        data = {
          check: "zipcode&land",
          val: valZip + "." + valLand
        };
        break;
      case "username":
        data = {
          check: "username",
          val: val
        };
        break;
      case "password":
        data = {
          check: "password",
          val: val
        };
        break;
      case "email":
        data = {
          check: "email",
          val: val
        };
        break;
      case "birth":
        $;

        data = {
          check: "birth",
          val: val
        };
        break;
    }

    $.ajax({
      contentType: "application/json",
      type: "POST",
      dataType: "json",
      url:
        "https://main.fda-backend.de/api/auth/register_external",
      data: JSON.stringify(data),
      beforeSend: function() {
        dhis
          .closest(".form-group")
          .find(".error-message")
          .hide();
      },
      success: function(data) {
        if (data.valid == 1 || data.login_url.length>0) {
          dhis
            .closest(".form-group")
            .removeClass("has-error")
            .addClass("has-success");
        }
      },
      error: function(error, status) {
        dhis
          .closest(".form-group")
          .removeClass("has-success")
          .addClass("has-error");
        dhis
          .closest(".form-group")
          .find(".error-message")
          .show()
          .html(error.responseJSON.message);
      }
    });
  });

  $(".api-check-finish-btn").on("click", function(e) {
    e.preventDefault();

    username = $(".api-username").val();
    passwort = $(".api-passwort").val();
    email = $(".api-email").val();
    geburtstag = $(".api-dob").val();
    plz = $(".api-zip").val();
    land = $(".api-land").val();

    data = {
      aid: aid,
      cid: cid,
      pid: pid,
      username: username,
      email: email,
      birth: geburtstag,
      zipcode: plz,
      country: land,
      password: passwort
    };

      data = {
        "email": email,
        "password": passwort,
        "profile_data": {
          "nickname": username,
          "canswers": {
            "birth": geburtstag,
            "country": land,
            "zip": plz
          }
        },
        "traffic_partner_id": pid,
        "source_id": aid,
        "click_id": cid
      };

    $.ajax({
      beforeSend: function() {
        $(".center-parent").show();
      },
      contentType: "application/json",
      type: "POST",
      dataType: "json",
      url:
        "https://main.fda-backend.de/api/auth/register_external",
      data: JSON.stringify(data),
      success: function(data) {
        $(".center-parent").hide();
        if (data.valid == 1 || data.login_url.length>0) {
          window.location.replace(data.login_url);
        } else {
          reloadPage();
        }
      },
      error: function(error, status) {
        $(".center-parent").hide();
        $(".submit-error").html("Fehler: " + error.responseJSON.message);
        $(".submit-error").show();
      }
    });
  });
});
